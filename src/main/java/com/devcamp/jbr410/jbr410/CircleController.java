package com.devcamp.jbr410.jbr410;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double Circle(@RequestParam(value = "radius", defaultValue = "1") double radius) {
        Circle circle = new Circle(radius);
        return circle.getArea();
    }  

    @CrossOrigin
    @GetMapping("/cylinder-volume")
    public double Cylinder(@RequestParam(value = "radius", defaultValue = "1") double radius,
                            @RequestParam(value = "height", defaultValue = "1") double height) {
        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }  
}
